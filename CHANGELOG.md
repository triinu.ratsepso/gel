# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.6.0] - 2022-10-20

### Added
- Add support for curved lanes
- Show application version in window title
- Projects can be used to group gels together
- Support sorting in the tables
- Allow to select location for SQLite database
- Add export to Excel
- Add button to toggle between synced and individual gel image lane widths
- Add image repository backends

### Changed
- Indicate current context on sidebar
- Use bundled icons
- Improve zoom indicator functionality
- Breaking: Adjusted SQL VIEW for reference samples
- Revised graph scrolling
- Revised line addition to gel image

## [0.5.0] - 2022-08-17

### Changed
- Breaking: new database format
- Improved background subtraction
- Model/view separation
- Navigation improvements
- UI changes
