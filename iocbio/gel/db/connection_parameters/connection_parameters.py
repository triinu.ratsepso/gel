class ConnectionParameters:
    """
    Interface for database connection parameters.
    """

    @staticmethod
    def to_connection_string(**connection_parameters) -> str:
        raise NotImplementedError

    @staticmethod
    def from_connection_string(connection_string: str) -> dict:
        return {}
