from __future__ import annotations
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from iocbio.gel.db.base import Entity


class EntityVisitor:
    def visit(self, entity: Entity):
        raise NotImplementedError
