-- Drop views if they exist in the order taking into account the dependencies
DROP VIEW IF EXISTS measurement_relative_value;
DROP VIEW IF EXISTS measurement_reference_value;
