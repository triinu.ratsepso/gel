import sqlparse

from alembic import op


def execute_sqlfile(filename: str) -> None:
    for s in sqlparse.parsestream(open(filename, "r")):
        statement = str(s).strip()
        op.execute(statement)
