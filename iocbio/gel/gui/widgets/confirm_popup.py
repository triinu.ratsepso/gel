from PySide6.QtWidgets import QMessageBox


class ConfirmPopup(QMessageBox):
    def __init__(self, title, question):
        super().__init__()
        self.setWindowTitle(title)
        self.setText(question)
        self.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        self.setIcon(QMessageBox.Question)

    def user_confirms(self):
        return self.exec() == QMessageBox.StandardButton.Ok
