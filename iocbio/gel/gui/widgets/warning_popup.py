from PySide6.QtWidgets import QMessageBox


class WarningPopup(QMessageBox):
    def __init__(self, title, message):
        super().__init__()
        self.setWindowTitle(title)
        self.setText(message)
        self.setStandardButtons(QMessageBox.Ok)
        self.setIcon(QMessageBox.Warning)
