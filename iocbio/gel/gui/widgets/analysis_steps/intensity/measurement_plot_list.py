from typing import Optional, List, Callable

from PySide6.QtCore import Slot

from iocbio.gel.application.application_state.state import ApplicationState
from iocbio.gel.application.event_registry import EventRegistry
from iocbio.gel.domain.gel_image_lane import GelImageLane
from iocbio.gel.domain.measurement import Measurement
from iocbio.gel.domain.measurement_lane import MeasurementLane
from iocbio.gel.domain.plot_region import PlotRegion
from iocbio.gel.gui.widgets.analysis_steps.intensity.intensity_plot import IntensityPlot
from iocbio.gel.gui.widgets.analysis_steps.intensity.plot_list import PlotList
from iocbio.gel.repository.measurement_lane_repository import MeasurementLaneRepository


class MeasurementPlotList(PlotList):
    """
    Scrollable list holding the individual intensity plots in the "Measurements" tab.
    """

    def __init__(
        self,
        event_registry: EventRegistry,
        application_state: ApplicationState,
        plot_provider: Callable[..., IntensityPlot],
        measurement_lane_repository: MeasurementLaneRepository,
    ):
        super().__init__(event_registry, application_state)
        self.plot_provider = plot_provider
        self.measurement_lane_repository = measurement_lane_repository

        self.measurement: Optional[Measurement] = None
        self.measurement_id = 0

        self.event_registry.measurement_selected.connect(self._on_measurement_selected)
        self.event_registry.measurement_deleted.connect(self._on_measurement_deleted)
        self.event_registry.gel_image_lane_selected.connect(self._on_lane_selection_change)
        self.event_registry.measurement_lane_added.connect(self._on_lane_added)
        self.event_registry.measurement_lane_deleted.connect(self._on_lane_deleted)
        self.event_registry.measurement_lane_updated.connect(self._on_lane_updated)

    def update_plots(self):
        if self._measurement_is_invalid(self.measurement):
            return self.clear_plots()
        super().update_plots()

    def get_plot_regions(self, image) -> List[PlotRegion]:
        """
        Map only those active lanes on the visual plots which are attached to the current measurement.
        """
        regions = super().get_plot_regions(image)

        lanes = {x.image_lane_id: x for x in self.measurement.active_lanes}

        selected = list(filter(lambda x: x.lane.id in lanes, regions))

        return list(map(lambda x: self._apply_range_to_area(x, lanes[x.lane.id]), selected))

    def create_plot(self, region: PlotRegion) -> IntensityPlot:
        """
        Create a plot which monitors the min/max ROI changes.
        """
        return self.plot_provider(
            plot_region=region,
            min_y=self.plot.min_h,
            max_y=self.plot.max_h,
            on_change_callback=self._on_roi_change,
        )

    @Slot(Measurement)
    def _on_measurement_selected(self, measurement: Measurement):
        """
        Change plots when a different measurement is selected.
        """
        if self._measurement_is_invalid(measurement):
            return self.clear()

        if self.measurement == measurement:
            return

        self.measurement = measurement
        self.measurement_id = measurement.id
        self.gel_image = measurement.image
        self.clear_plots()
        self.update_plots()

    @Slot(int)
    def _on_measurement_deleted(self, measurement_id):
        if self.measurement_id == measurement_id:
            return self.clear()

    @Slot(GelImageLane)
    def _on_lane_selection_change(self, image_lane: GelImageLane):
        """
        Update visible plots when a new lane is attached to the measurement.
        """
        if not self.measurement:
            return self.clear_plots()

        for lane in self.measurement.active_lanes:
            if lane.image_lane_id == image_lane.id:
                self.measurement_lane_repository.delete(lane)
                return

        image = self.gel_image.get_plot_data()
        plot_region = self._lane_to_plot_region(image_lane, image)
        plot_region.calculate_area()

        lane = MeasurementLane(
            gel_id=image_lane.gel_id,
            image_id=image_lane.image_id,
            image_lane_id=image_lane.id,
            measurement_id=self.measurement.id,
            value=plot_region.area,
            comment="",
            is_success=True,
        )

        self.measurement_lane_repository.add(lane)

    @Slot(MeasurementLane)
    def _on_lane_added(self, lane: MeasurementLane):
        if lane.measurement_id != self.measurement_id:
            return
        self.update_plots()

    @Slot(int)
    def _on_lane_deleted(self, _):
        if self._measurement_is_invalid(self.measurement):
            return
        if len(self.plot.regions) != len(self.measurement.active_lanes):
            self.update_plots()

    @Slot(MeasurementLane)
    def _on_lane_updated(self, lane: MeasurementLane):
        if lane.measurement_id != self.measurement_id:
            return
        self.update_plots()

    def _on_roi_change(self, plot_region: PlotRegion):
        """
        Propagate widget changes to the database.
        """
        lanes = {x.image_lane_id: x for x in self.measurement.active_lanes}
        if plot_region.lane.id not in lanes:
            return

        changed_lane = lanes[plot_region.lane.id]
        changed_lane.min = plot_region.min_limit
        changed_lane.max = plot_region.max_limit
        changed_lane.value = plot_region.area
        self.measurement_lane_repository.update(changed_lane)

    def _replace_image(self, gel_image):
        super()._replace_image(gel_image)
        self.measurement = None

    @staticmethod
    def _apply_range_to_area(
        plot_region: PlotRegion, measurement_lane: MeasurementLane
    ) -> PlotRegion:
        if measurement_lane.min is not None:
            plot_region.min_limit = measurement_lane.min
        if measurement_lane.max is not None:
            plot_region.max_limit = measurement_lane.max
        plot_region.calculate_area()
        return plot_region

    @staticmethod
    def _measurement_is_invalid(measurement: Measurement) -> bool:
        return measurement is None or measurement.id is None
