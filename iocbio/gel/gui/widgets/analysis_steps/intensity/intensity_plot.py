import pyqtgraph as pg
import numpy as np
from PySide6.QtCore import QRect
from PySide6.QtWidgets import QSizePolicy

from iocbio.gel.application.event_consumer import EventConsumer
from iocbio.gel.domain.plot_region import PlotRegion
from iocbio.gel.gui.widgets.analysis_steps.intensity.zero_line import ZeroLine


class IntensityPlot(pg.PlotWidget, EventConsumer):
    """
    Base class for the widgets displaying the individual intensity plots.
    """

    INITIAL_ZERO_LINE_PERCENTILE = 1

    def __init__(self, plot_region: PlotRegion, min_y, max_y, parent=None):
        pg.PlotWidget.__init__(self, parent, background="white")
        EventConsumer.__init__(self)

        self.setLimits(minXRange=10, minYRange=10)

        self.plot_region = plot_region
        self.min_y = min_y
        self.max_y = max_y
        self.max_x = plot_region.image.shape[0] - 1

        points = (
            self._initial_zero_line_points() if len(plot_region.points) < 2 else plot_region.points
        )

        self.setMinimumSize(100, 100)
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        self.setYRange(min_y, max_y)
        self._graph_pen = pg.mkPen({"color": "#005AEB", "width": 2})
        self.intensities_graph = self.plot(
            list(range(0, len(self.plot_region.intensities))),
            self.plot_region.intensities,
            pen=self._graph_pen,
        )

        self.zero_line = self.create_zero_line(points)
        self.addItem(self.zero_line)

    def set_model(self, plot_region: PlotRegion, min_y, max_y):
        """
        Change visuals to match new plot region.
        """
        if self.min_y != min_y or self.max_y != self.max_y:
            self.setYRange(min_y, max_y)
            self.min_y = min_y
            self.max_y = max_y

        self.max_x = plot_region.image.shape[0] - 1

        if self.plot_region.lane_id != plot_region.lane_id:
            self.setYRange(min_y, max_y)
            self.setXRange(0, self.max_x)

        self.plot_region = plot_region

        x_range = list(range(0, len(self.plot_region.intensities)))
        y_range = self.plot_region.intensities
        if self._intensities_changed(self.intensities_graph.getData(), [x_range, y_range]):
            self.removeItem(self.intensities_graph)
            self.intensities_graph = self.plot(x_range, y_range, pen=self._graph_pen)

        points = (
            self._initial_zero_line_points() if len(plot_region.points) < 2 else plot_region.points
        )
        if not np.array_equal(points, self.zero_line.as_points()):
            self.zero_line.setPoints(points)

        self.zero_line.maxBounds = self._max_bounds()

    def create_zero_line(self, points) -> ZeroLine:
        """
        To be implemented by children.
        """
        pass

    def update_title(self):
        """
        Update the visual title on the plot.
        """
        self.setTitle(f"L{self.plot_region.lane.gel_lane.lane}")

    def hasHeightForWidth(self) -> bool:
        return True

    def heightForWidth(self, w: int) -> int:
        return max(100, int(w / 1.6))

    def _max_bounds(self):
        return QRect(0, self.min_y, self.max_x, self.max_y)

    def _initial_zero_line_points(self):
        low_y = np.percentile(self.plot_region.intensities, self.INITIAL_ZERO_LINE_PERCENTILE)
        return [[0, low_y], [self.max_x, low_y]]

    @staticmethod
    def _intensities_changed(current_data, new_data):
        return not np.array_equal(current_data[0], new_data[0]) or not np.array_equal(
            current_data[1], new_data[1]
        )
