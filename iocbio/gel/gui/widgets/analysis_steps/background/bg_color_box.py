from PySide6.QtWidgets import QGroupBox, QRadioButton, QVBoxLayout


class BgColorBox(QGroupBox):
    """
    Widget for selecting between light and dark image background.
    """

    def __init__(self, name, *args, **kwargs):
        super(BgColorBox, self).__init__(name, *args, **kwargs)

        self.is_dark_button = QRadioButton("is dark")
        self.is_light_button = QRadioButton("is light")
        self.is_light_button.setChecked(True)

        layout = QVBoxLayout()
        layout.addWidget(self.is_dark_button)
        layout.addWidget(self.is_light_button)
        self.setLayout(layout)

    def set_dark(self, is_dark):
        self.is_dark_button.setChecked(is_dark)

    def is_dark(self):
        return self.is_dark_button.isChecked()
