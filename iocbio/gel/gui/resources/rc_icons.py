# Resource object code (Python 3)
# Created by: object code
# Created by: The Resource Compiler for Qt version 6.3.1
# WARNING! All changes made in this file will be lost!

from PySide6 import QtCore

qt_resource_data = b"\
\x00\x00\x01\xb8\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 width=\x2296\
\x22 height=\x2296\x22 vi\
ewBox=\x220 0 24 24\
\x22 fill=\x22none\x22 st\
roke=\x22black\x22 str\
oke-width=\x222\x22 st\
roke-linecap=\x22ro\
und\x22 stroke-line\
join=\x22round\x22 cla\
ss=\x22feather feat\
her-coffee\x22><pat\
h d=\x22M18 8h1a4 4\
 0 0 1 0 8h-1\x22><\
/path><path d=\x22M\
2 8h16v9a4 4 0 0\
 1-4 4H6a4 4 0 0\
 1-4-4V8z\x22></pat\
h><line x1=\x226\x22 y\
1=\x221\x22 x2=\x226\x22 y2=\
\x224\x22></line><line\
 x1=\x2210\x22 y1=\x221\x22 \
x2=\x2210\x22 y2=\x224\x22><\
/line><line x1=\x22\
14\x22 y1=\x221\x22 x2=\x221\
4\x22 y2=\x224\x22></line\
></svg>\
\x00\x00\x01$\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 width=\x2296\
\x22 height=\x2296\x22 vi\
ewBox=\x220 0 24 24\
\x22 fill=\x22none\x22 st\
roke=\x22black\x22 str\
oke-width=\x222\x22 st\
roke-linecap=\x22ro\
und\x22 stroke-line\
join=\x22round\x22 cla\
ss=\x22feather feat\
her-x\x22><line x1=\
\x2218\x22 y1=\x226\x22 x2=\x22\
6\x22 y2=\x2218\x22></lin\
e><line x1=\x226\x22 y\
1=\x226\x22 x2=\x2218\x22 y2\
=\x2218\x22></line></s\
vg>\
\x00\x00\x01j\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 width=\x2296\
\x22 height=\x2296\x22 vi\
ewBox=\x220 0 24 24\
\x22 fill=\x22none\x22 st\
roke=\x22black\x22 str\
oke-width=\x222\x22 st\
roke-linecap=\x22ro\
und\x22 stroke-line\
join=\x22round\x22 cla\
ss=\x22feather feat\
her-image\x22><rect\
 x=\x223\x22 y=\x223\x22 wid\
th=\x2218\x22 height=\x22\
18\x22 rx=\x222\x22 ry=\x222\
\x22></rect><circle\
 cx=\x228.5\x22 cy=\x228.\
5\x22 r=\x221.5\x22></cir\
cle><polyline po\
ints=\x2221 15 16 1\
0 5 21\x22></polyli\
ne></svg>\
\x00\x00\x010\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 width=\x2296\
\x22 height=\x2296\x22 vi\
ewBox=\x220 0 24 24\
\x22 fill=\x22none\x22 st\
roke=\x22black\x22 str\
oke-width=\x222\x22 st\
roke-linecap=\x22ro\
und\x22 stroke-line\
join=\x22round\x22 cla\
ss=\x22feather feat\
her-folder\x22><pat\
h d=\x22M22 19a2 2 \
0 0 1-2 2H4a2 2 \
0 0 1-2-2V5a2 2 \
0 0 1 2-2h5l2 3h\
9a2 2 0 0 1 2 2z\
\x22></path></svg>\
\x00\x00\x013\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 width=\x2296\
\x22 height=\x2296\x22 vi\
ewBox=\x220 0 24 24\
\x22 fill=\x22none\x22 st\
roke=\x22black\x22 str\
oke-width=\x222\x22 st\
roke-linecap=\x22ro\
und\x22 stroke-line\
join=\x22round\x22 cla\
ss=\x22feather feat\
her-arrow-right\x22\
><line x1=\x225\x22 y1\
=\x2212\x22 x2=\x2219\x22 y2\
=\x2212\x22></line><po\
lyline points=\x221\
2 5 19 12 12 19\x22\
></polyline></sv\
g>\
"

qt_resource_name = b"\
\x00\x05\
\x00o\xa6S\
\x00i\
\x00c\x00o\x00n\x00s\
\x00\x07\
\x0c\xb8\xae\x02\
\x00f\
\x00e\x00a\x00t\x00h\x00e\x00r\
\x00\x0a\
\x0c\xbd\x11G\
\x00c\
\x00o\x00f\x00f\x00e\x00e\x00.\x00s\x00v\x00g\
\x00\x05\
\x00{Z\xc7\
\x00x\
\x00.\x00s\x00v\x00g\
\x00\x09\
\x07\xd8\xba\xa7\
\x00i\
\x00m\x00a\x00g\x00e\x00.\x00s\x00v\x00g\
\x00\x0a\
\x0a\xc8\xf6\x87\
\x00f\
\x00o\x00l\x00d\x00e\x00r\x00.\x00s\x00v\x00g\
\x00\x0f\
\x0f\x22iG\
\x00a\
\x00r\x00r\x00o\x00w\x00-\x00r\x00i\x00g\x00h\x00t\x00.\x00s\x00v\x00g\
"

qt_resource_struct = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x02\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x10\x00\x02\x00\x00\x00\x05\x00\x00\x00\x03\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00>\x00\x00\x00\x00\x00\x01\x00\x00\x01\xbc\
\x00\x00\x01\x82\xed\xc5X\xdf\
\x00\x00\x00N\x00\x00\x00\x00\x00\x01\x00\x00\x02\xe4\
\x00\x00\x01\x82\xed\xc5I\xe2\
\x00\x00\x00f\x00\x00\x00\x00\x00\x01\x00\x00\x04R\
\x00\x00\x01\x82\xed\xc5H\x1b\
\x00\x00\x00$\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
\x00\x00\x01\x82\xed\xc5DL\
\x00\x00\x00\x80\x00\x00\x00\x00\x00\x01\x00\x00\x05\x86\
\x00\x00\x01\x82\xed\xc5A=\
"


def qInitResources():
    QtCore.qRegisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)


def qCleanupResources():
    QtCore.qUnregisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)


qInitResources()
