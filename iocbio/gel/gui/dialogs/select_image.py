from PySide6.QtWidgets import QDialog


class SelectImage(QDialog):
    """
    Dialog interface for image selection.
    """

    def __init__(self):
        super().__init__()

    def get_path(self) -> str:
        raise NotImplementedError
