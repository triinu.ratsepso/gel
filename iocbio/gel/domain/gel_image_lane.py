import json

from sqlalchemy import Column, Integer, Text, ForeignKeyConstraint
from sqlalchemy.orm import relationship

from iocbio.gel.db.base import Base, Entity
from iocbio.gel.db.entity_visitor import EntityVisitor
from iocbio.gel.domain.gel_lane import GelLane
from iocbio.gel.domain.curved_lane_region import CurvedLaneRegion as LaneRegion


class GelImageLane(Entity, Base):
    """
    Entity holding the data for user-placed lanes on the gel image.
    """

    __tablename__ = "image_lane"
    __table_args__ = (
        ForeignKeyConstraint(["image_id", "gel_id"], ["image.id", "image.gel_id"]),
        ForeignKeyConstraint(["gel_lane_id", "gel_id"], ["gel_lane.id", "gel_lane.gel_id"]),
    )

    id = Column(Integer, primary_key=True)
    gel_id = Column(Integer, nullable=False)
    gel_lane_id = Column(Integer, nullable=False)
    image_id = Column(Integer, nullable=False)
    region = Column(Text)
    zero_line_points = Column(Text)

    gel_image = relationship(
        "GelImage", back_populates="lanes", uselist=False, overlaps="gel_image_lanes"
    )
    gel_lane: GelLane = relationship(
        "GelLane", back_populates="gel_image_lanes", uselist=False, overlaps="gel_image,lanes"
    )
    measurement_lanes = relationship("MeasurementLane", back_populates="image_lane")

    @property
    def width(self) -> int:
        return self.get_region().width

    def get_region(self) -> LaneRegion:
        return LaneRegion.deserialize(self.region)

    def set_region(self, lane_region: LaneRegion):
        self.region = lane_region.serialize()

    def get_zero_line(self) -> list:
        if self.zero_line_points:
            return list(json.loads(self.zero_line_points))
        return []

    def set_zero_line(self, region: list):
        self.zero_line_points = json.dumps(region)

    def accept(self, visitor: EntityVisitor):
        for lane in self.measurement_lanes:
            lane.accept(visitor)
        visitor.visit(self)
