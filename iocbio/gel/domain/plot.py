import math
import numpy as np

from typing import List

from iocbio.gel.domain.plot_region import PlotRegion


class Plot:
    """
    Wrapper for a collection of intensity plots for placed lanes.
    """

    def __init__(self, image):
        self.image = image
        self.regions: List[PlotRegion] = []
        self.areas = []
        self.percentages = []
        self.min_h = 0
        self.max_h = 0
        self.total = 0

    def calculate_areas(self):
        """
        Calculates selected region areas and their relative sizes in percentages.
        """
        if not self.regions:
            return

        self.min_h = math.ceil(np.min([region.min_intensity for region in self.regions]))
        self.max_h = math.floor(np.max([region.max_intensity for region in self.regions]))

        self.areas = [region.area for region in self.regions]
        total = np.sum(self.areas)
        if total == 0:
            self.percentages = [0] * len(self.areas)
        else:
            self.percentages = [area / total * 100 for area in self.areas]

        for index, region in enumerate(self.regions):
            region.area = round(self.areas[index], 3)
            region.percentage = round(self.percentages[index], 3)
