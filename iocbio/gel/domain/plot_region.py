import numpy as np
from PySide6.QtCore import QLineF

from iocbio.gel.domain.gel_image_lane import GelImageLane
from iocbio.gel.domain.curved_lane_region import CurvedLaneRegion as LaneRegion


class PlotRegion:
    """
    A user-selected region on the pixel intensity plot of a lane.
    Holds the integration bounds and zero-line points placed by the user.
    """

    def __init__(self, image, lane: GelImageLane, lane_region: LaneRegion):
        self.image = image
        self.intensities = lane_region.calculate_intensities(image)
        self.min_intensity = np.min(self.intensities)
        self.max_intensity = np.max(self.intensities)
        self.lane = lane
        self.lane_id = lane.id
        self.area = np.sum(self.intensities)
        self.percentage = 0
        self.min_limit = 0
        self.max_limit = self.image.shape[0]
        self.points = []

    def calculate_area(self):
        """
        Finds the area between integration bounds and above the zero-line.
        """
        points = self.points.copy()

        intensities = self.intensities[self.min_limit : self.max_limit]
        area = np.sum(intensities)

        if len(points) < 2 or area == 0:
            self.area = area
            return

        min_limit, max_limit = self._make_limit_lines()

        in_limit = []
        for i in range(len(points) - 1):
            in_limit.extend(
                self._get_points_in_limit(
                    points[i][0],
                    points[i + 1][1],
                    points[i + 1][0],
                    points[i][1],
                    min_limit,
                    max_limit,
                )
            )

        z_area = 0
        for i in range(len(in_limit) - 1):
            z_area += self._right_trapezoid_area(
                in_limit[i][0], in_limit[i + 1][1], in_limit[i + 1][0], in_limit[i][1]
            )

        self.area = max(area - z_area, 0)

    def _make_limit_lines(self):
        a = np.max(self.points) + 10
        b = np.min(self.points) - 10
        return QLineF(self.min_limit, a, self.min_limit, b), QLineF(
            self.max_limit, a, self.max_limit, b
        )

    def _get_points_in_limit(self, x1, y1, x2, y2, min_line, max_line) -> list:
        """
        For a given segment return points that are either within the bounds or on the bound intersections.
        """
        line = QLineF(x1, y1, x2, y2)
        min_point = self._get_bounded_intersection_point(min_line, line)
        max_point = self._get_bounded_intersection_point(max_line, line)

        if min_point and max_point:
            return [(min_point.x(), min_point.y()), (max_point.x(), max_point.y())]
        if min_point:
            return [(min_point.x(), min_point.y()), (x2, y2)]
        if max_point:
            return [(x1, y1), (max_point.x(), max_point.y())]

        points = []
        if self.min_limit < x1 < self.max_limit:
            points.append((x1, y1))
        if self.min_limit < x2 < self.max_limit:
            points.append((x2, y2))
        return points

    @staticmethod
    def _right_trapezoid_area(x1, y1, x2, y2):
        return (y1 + y2) * (abs(x1 - x2) / 2)

    @staticmethod
    def _get_bounded_intersection_point(a: QLineF, b: QLineF):
        intersection, point = a.intersects(b)
        if intersection == QLineF.IntersectionType.BoundedIntersection:
            return point
        return None
