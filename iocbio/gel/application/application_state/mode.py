from enum import Enum


class ApplicationMode(Enum):
    """
    Options for different levels of restrictions on using the application.
    """

    EDITING = "EDITING"
    VIEWING = "VIEWING"
    OFFLINE = "OFFLINE"
