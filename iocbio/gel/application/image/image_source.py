from enum import Enum


class ImageSource(Enum):
    """
    Available options for the image source.
    """

    LOCAL = "local"
    OMERO = "omero"
