from enum import Enum, auto


class ImageState(Enum):
    LOADING = auto()
    READY = auto()
    MISSING = auto()
