from PySide6.QtCore import QObject, Signal

from iocbio.gel.application.thread.job import Job


class Signals(QObject):
    jobs_changed = Signal(int)


class Worker(QObject):
    def __init__(self, *key: str):
        super().__init__()
        self.signals = Signals()

    @property
    def jobs(self):
        return 0

    def start(self, job: Job) -> None:
        raise NotImplementedError
