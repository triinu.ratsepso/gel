# Installation and update

There are several ways to install the software. The application
requires python3.

To avoid interference with the other packages, it is recommended to
use virtual environments, as described below.

We recommend to install the software using automatic installation
scripts provided for Linux/Mac and Windows.

## Releases

All releases are listed at
[Releases](https://gitlab.com/iocbio/gel/-/releases). Releases are
distributed as executable (for Windows) and through The Python Package
Index (PyPI).

## Linux/Mac

To use automatic installation script for Linux/Mac make first sure that you have
latest `pip` installed by running:

```
python3 -m pip install --user --upgrade pip
```

Then open terminal and go to folder where you like to install the program.
Then run following command:

```
curl https://gitlab.com/iocbio/gel/-/raw/master/install.sh | bash
```

or

```
wget -qO - https://gitlab.com/iocbio/gel/-/raw/master/install.sh | bash
```
and run by
```
iocbio-gel/bin/iocbio-gel
```

## Windows

Note that for Windows we provide also executable that is available under
[Releases](https://gitlab.com/iocbio/gel/-/releases). 

To install IOCBIO Gel software using automatic installation script
make sure that you have Python installed. More information about
installing Python in Windows see [Python for
Beginners](https://docs.microsoft.com/en-us/windows/python/beginners).
In addition, install Microsoft Visual C++ Redistributable for Visual
Studio 2015, 2017 and 2019. Respective installer can be found
[here](https://docs.microsoft.com/en-US/cpp/windows/latest-supported-vc-redist?view=msvc-160).

When Python is installed open PowerShell and make first sure that
[Get-ExecutionPolicy](https://go.microsoft.com/fwlink/?LinkID=135170)
is not Restricted. We suggest using `Bypass` to bypass the policy to
get things installed or `AllSigned` for quite a bit more
security. First, run `Get-ExecutionPolicy` in PowerShell. In case, it
returns `Restricted`, then run `Set-ExecutionPolicy AllSigned` or
`Set-ExecutionPolicy Bypass -Scope Process`.  When this is all set you
can install the program by running following command:

```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/iocbio/gel/-/raw/master/install.ps1'))
```


## pip with virtual environment

Sometimes packages for different applications can cause
incompatibilities. To avoid it, you could use virtual environment for
the software installation. To create virtual python environment, run

```
python -m venv iocbio-gel
```

This will create folder `iocbio-gel` and install scripts, such as
`pip`, into it. To use the environment, call `pip` from that folder
and install iocbio-gel into it

```
iocbio-gel/bin/pip install iocbio.gel
```
and run by
```
iocbio-gel/bin/iocbio-gel
```

Same in Windows, using Anaconda Python PowerShell:
```
python.exe -m venv iocbio-gel
.\iocbio-gel\Scripts\python.exe
.\iocbio-gel\Scripts\pip install iocbio.gel
```
and use by
```
.\iocbio-gel\Scripts\Activate.ps1
iocbio-gel.exe
```

### pip

If not available in the system, you can replace `pip3` command
below with `python3 -m pip`.

To install published version, run

```
pip3 install --user iocbio.gel
```
This will install all dependencies and it is expected to add a command `iocbio-gel` into your `PATH`.
If the command is missing after installation, check whether the default location
of `pip3` installation is in your path. For Linux, it should be `~/.local/bin`.

Its possible to install from Git repository directly:
```
pip3 install --user git+https://gitlab.com/iocbio/gel
```

For development, use

```
pip3 install --user -e .
```

in the source directory. To install the current version from the source, use

```
pip3 install --user .
```

Note that `--user` is recommended to avoid messing up the system
packages.
