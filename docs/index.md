# IOCBIO Gel: Software for Gel image analysis

IOCBIO Gel was originally developed for Western Blot gel image analysis. However, it can be used
for any sample analysis technique that would result in similar images where each sample is allocated
a "well" with the intensity of blobs in the wells corresponding to sample content. 

IOCBIO Gel was designed to fit into routine by simplifying preprocessing of the images, such as 
background correction, cataloging the results and providing simple access to analysis data. 
The statistical analysis of the samples is expected to be run by the user through other software. 
For that, either database access can be used or data can be exported into spreadsheet.

## Links

- [Project page](https://gitlab.com/iocbio/gel)
- [Installation instructions](install.md)
- [Releases](https://gitlab.com/iocbio/gel/-/releases)
- [Issues](https://gitlab.com/iocbio/gel/issues)
- [Development notes](development.md)

## Copyright

Copyright (C) 2022, Authors of the application as listed in software repository.

Software license: the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.
