# IOCBIO Gel: Software for Gel image analysis

IOCBIO Gel was originally developed for Western Blot gel image analysis. However, it can be used
for any sample analysis technique that would result in similar images where each sample is allocated
a "well" with the intensity of blobs in the wells corresponding to sample content. 

IOCBIO Gel is a free software released under the GNU General Public
License (GPL), see the file [`LICENSE`](LICENSE.md) for details.

See [IOCBIO Gel homepage](https://iocbio.gitlab.io/gel) for
brief description, links and installation instructions.


## Releases

All releases are listed at
[Releases](https://gitlab.com/iocbio/gel/-/releases). Releases are
distributed as executable (for Windows) and through The Python Package
Index (PyPI). 

## Copyright

Copyright (C) 2022, Authors of the application as listed under [AUTHORS](AUTHORS.md).

Software license: the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version. See [`LICENSE`](LICENSE.md) for details

Software was developed with/in the Laboratory of Systems Biology, Department of
Cybernetics, School of Science, Tallinn University of Technology.
